# iSearch
## Folder Structure
- **core** - Includes shared classes that multiple modules uses
- **features** - Includes multiple sub features of the App (simulates multi module approach). A Feature module only does one thing, for example Search feature handles searching and showing the search result, while details submodule only is aware on how to display a detailed information of the Media.

## Packages
- **data** - contains data specific models and also the database entities and DAO. Models and DAO should never be directly access by the presentation layer (Activity/Fragment/Views).
- **di** - contains Modules to provide instances needed for Dependency Injection
- **domain** - contains Models shared by the Domain layer and the Presentation Layer. The models inside domain is accessible by viewmodels (as LiveData) under domain.
- **presentation** - contains application/activities/fragments/views etc

## Data Flow
**From API**
Retrofit -> Service -> Repository -> ViewModel -> Activity

**From Database**
Database -> DAO -> Repository -> ViewModel -> Activity

## Dependency Injection
Dependencies are injected via [Hilt](https://developer.android.com/training/dependency-injection/hilt-android).

**ViewModel** expects the **abstract class of a Repository** and not the concrete class, this makes it mockable and testable.

**ViewModel** expects the **abstract class of a Service/DAO** and not the concrete class, this makes it mockable and testable.

## Separation of Models
Each feature/presentation has its own expected Model this is to inverse dependency and decouple each features as evident from DetailsMediaItem and SearchMediaItem.
