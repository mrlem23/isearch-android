package com.sample.lem.isearch.features.search.presentation.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sample.lem.isearch.R

/**
 * @param searchHistory
 * @param onSearchHistoryItemClicked called when an item is clicked
 * @param onItemDeleteClicked called when the delete icon on a history item is clicked
 */
class SearchHistoryListAdapter(
    private val searchHistory: List<String>,
    private val onSearchHistoryItemClicked: ((query: String) -> Unit)?,
    private val onItemDeleteClicked: ((query: String) -> Unit)?
) : RecyclerView.Adapter<SearchHistoryListAdapter.SearchHistoryItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchHistoryItemViewHolder =
        LayoutInflater.from(parent.context).run {
            inflate(R.layout.item_search_history, parent, false).run {
                SearchHistoryItemViewHolder(this)
            }
        }

    override fun onBindViewHolder(holder: SearchHistoryItemViewHolder, position: Int) {
        searchHistory[position].also { query ->
            holder.tvText.text = query
            holder.tvText.setOnClickListener {
                onSearchHistoryItemClicked?.invoke(query)
            }
            holder.ivDelete.setOnClickListener {
                onItemDeleteClicked?.invoke(query)
            }
        }
    }

    override fun getItemCount(): Int = searchHistory.size

    class SearchHistoryItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvText: TextView by lazy { itemView.findViewById(R.id.tvText) }
        val ivDelete : View by lazy { itemView.findViewById(R.id.ivDelete) }
    }
}