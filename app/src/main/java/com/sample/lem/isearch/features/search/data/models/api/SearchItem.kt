package com.sample.lem.isearch.features.search.data.models.api

/**
 * Response model from iTunes
 *
 * @param trackName
 * @param collectionPrice
 * @param collectionName
 * @param trackPrice
 * @param artworkUrl60 used instead of artworkUrl30 since artworkUrl30 is not present always
 * @param kind can be any of the ff (book, album, coached-audio, feature-movie, interactive- booklet, music-video, pdf podcast, podcast-episode, software-package, song, tv- episode, artistFor example: song.)
 * @param currency interpolated with trackPrice later
 * @param primaryGenreName
 * @param longDescription
 */
data class SearchItem(
    val trackName :String?,
    val collectionPrice :String?,
    val collectionName : String?,
    val trackPrice : String?,
    val artworkUrl60 : String?,
    var kind : String,
    var currency : String,
    var primaryGenreName : String,
    var longDescription : String?
)