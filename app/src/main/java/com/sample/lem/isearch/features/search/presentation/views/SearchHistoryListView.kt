package com.sample.lem.isearch.features.search.presentation.views

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sample.lem.isearch.features.search.presentation.adapters.SearchHistoryListAdapter


/**
 * Compound view which shows list of last queries
 */
class SearchHistoryListView(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
    RecyclerView(context, attrs, defStyleAttr) {

    constructor(context: Context) : this(context, null, 0)

    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, 0)

    /**
     * Lambda function called when an item is clicked
     *
     * @param query
     */
    private var onHistoryItemClicked : (query : String) -> Unit = {}

    /**
     * Lambda function called when delete on search item is clicked
     *
     * @param query
     */
    private var onDeleteHistoryItemClicked : (query : String) -> Unit = {}

    /**
     * Once inflation is finished setup this recyclerview
     */
    override fun onFinishInflate() {
        super.onFinishInflate()
        adapter = SearchHistoryListAdapter(arrayListOf(), onHistoryItemClicked, onDeleteHistoryItemClicked)
        layoutManager = LinearLayoutManager(context)
    }

    /**
     * Set/Override default [com.sample.lem.isearch.features.search.presentation.views.SearchHistoryListView.onHistoryItemClicked]
     */
    fun setOnItemClicked(listener : (query : String) -> Unit){
        onHistoryItemClicked = listener
    }

    /**
     * Set/Override default [com.sample.lem.isearch.features.search.presentation.views.SearchHistoryListView.onDeleteHistoryItemClicked]
     */
    fun setDeleteItemClicked(listener : (query : String) -> Unit){
        onDeleteHistoryItemClicked = listener
    }

    /**
     * Update data of Recyclerview, automatically hides or show this view if data is empty or not
     */
    fun updateData(newList : List<String>) {
        adapter = SearchHistoryListAdapter(newList, onHistoryItemClicked, onDeleteHistoryItemClicked)
        if (newList.isEmpty()) {
            visibility = GONE
        } else {
            visibility = VISIBLE
        }
    }
}