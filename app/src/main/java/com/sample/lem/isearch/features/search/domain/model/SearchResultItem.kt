package com.sample.lem.isearch.features.search.domain.model

/**
 * Model specifically for ViewModel and presentation layer
 *
 * Models under [com.sample.lem.isearch.features.search.domain.model] is the only models
 * that the presentation layer should be aware of
 */
data class SearchResultItem(
    val price : String,
    val previewUrl : String?,
    val fullImgUrl : String?,
    val title : String,
    val subTitle : String,
    val description : String
)