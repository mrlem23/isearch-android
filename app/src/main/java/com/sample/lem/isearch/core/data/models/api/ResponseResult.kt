package com.sample.lem.isearch.core.data.models.api

/**
 * Wrapper model for API responses
 *
 * @param state
 * @param data body of API response
 */
data class ResponseResult<K>(
    val state : RequestState,
    val data : K
)
