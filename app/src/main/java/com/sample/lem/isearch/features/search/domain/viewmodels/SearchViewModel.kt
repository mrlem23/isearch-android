package com.sample.lem.isearch.features.search.domain.viewmodels

import android.app.Application
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.sample.lem.isearch.core.data.models.api.RequestState
import com.sample.lem.isearch.core.data.models.api.ResponseResult
import com.sample.lem.isearch.features.search.data.models.api.SearchItem
import com.sample.lem.isearch.features.search.data.service.ItunesSearchRepository
import com.sample.lem.isearch.features.search.data.service.SearchHistoryRepository
import com.sample.lem.isearch.features.search.domain.model.SearchResultItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    application: Application,
    private val itunesSearchService: ItunesSearchRepository,
    private val searchHistoryRepository: SearchHistoryRepository
) : AndroidViewModel(application) {

    /**
     * Delete item from table
     *
     * @param query query which will be used for deletion
     * @param count last number of items to check for deletion
     */
    fun removeFromSearchHistory(query: String, count: Int = 3) : LiveData<List<String>> {
        val searchHistory: MutableLiveData<List<String>> =
            MutableLiveData<List<String>>()

        viewModelScope.launch(Dispatchers.IO){
            searchHistoryRepository.deleteQuery(query, count)
            searchHistoryRepository.getLatestSearch(count).apply {
                searchHistory.postValue(this)
            }
        }

        return searchHistory
    }

    /**
     * Gets search history base on
     *
     * @param count
     */
    fun getSearchHistory(count: Int): LiveData<List<String>> {
        val searchHistory: MutableLiveData<List<String>> =
            MutableLiveData<List<String>>()
        viewModelScope.launch(Dispatchers.IO) {
            searchHistoryRepository.getLatestSearch(count).apply {
                searchHistory.postValue(this)
            }
        }
        return searchHistory
    }

    /**
     * Search media item
     *
     * @param query term to use
     * @param country country to use base on (ISO Countery Codes)[http://en.wikipedia.org/wiki/%20ISO_3166-1_alpha-2]
     */
    fun search(
        query: String,
        country: String = "US"
    ): LiveData<ResponseResult<ArrayList<SearchResultItem>>> {

        val result: MutableLiveData<ResponseResult<ArrayList<SearchResultItem>>> =
            MutableLiveData<ResponseResult<ArrayList<SearchResultItem>>>()

        viewModelScope.launch(Dispatchers.IO) {
            searchHistoryRepository.saveSearch(query)
            itunesSearchService.search(query, country).run {
                when {
                    isSuccessful -> {
                        this.body()?.apply {
                            if (resultCount == 0) {
                                result.postValue(
                                    ResponseResult(
                                        state = RequestState.Success,
                                        data = arrayListOf()
                                    )
                                )
                            } else {
                                result.postValue(
                                    ResponseResult(
                                        state = RequestState.Success,
                                        data = convertSearchItem(results)
                                    )
                                )
                            }
                        }
                    }
                    else -> {
                        result.postValue(
                            ResponseResult(
                                state = RequestState.Failed,
                                data = arrayListOf()
                            )
                        )
                    }
                }
            }
        }

        return result;
    }

    /**
     * Converts data model to expected domain model before passing to presentation layer
     */
    private fun convertSearchItem(items: List<SearchItem>): ArrayList<SearchResultItem> {
        val searchResultItems: ArrayList<SearchResultItem> =
            ArrayList()

        items.iterator().forEach { item ->
            SearchResultItem(
                price = "${item.currency} ${item.trackPrice ?: item.collectionPrice}",
                previewUrl = item.artworkUrl60?.run {
                    getSpecificPreviewSize(100, 100, this)
                },
                fullImgUrl = item.artworkUrl60?.run {
                    getSpecificPreviewSize(300, 300, this)
                },
                subTitle = item.primaryGenreName,
                title = (item.trackName ?: item.collectionName) ?: "Unknown",
                description = item.longDescription ?: "No description was provided"
            ).apply {
                searchResultItems.add(this)
            }
        }

        return searchResultItems;
    }

    /**
     * Helper to parse/create a link for a specific preview size
     */
    private fun getSpecificPreviewSize(height: Int, width: Int, url: String): String {
        return Uri.parse(url).let { uri ->
            ArrayList(uri.pathSegments).run {
                removeAt(size - 1)
                add("${height}x${width}bb.jpg")
                "${uri.scheme}://${uri.host}/${joinToString("/")}"
            }
        }
    }
}

