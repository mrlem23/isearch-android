package com.sample.lem.isearch.core.presentation.activities

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager

/**
 * Extension function to easily dismiss keyboard if needed
 */
fun Activity.dismissKeyboard() {
    val inputMethodManager = getSystemService( Context.INPUT_METHOD_SERVICE ) as InputMethodManager
    if( inputMethodManager.isAcceptingText )
        inputMethodManager.hideSoftInputFromWindow( this.currentFocus?.windowToken, 0)
}