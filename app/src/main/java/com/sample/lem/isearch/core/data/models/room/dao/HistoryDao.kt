package com.sample.lem.isearch.core.data.models.room.dao

import androidx.room.Insert
import androidx.room.Query

/**
 * Helper DAO to avoid boilerplate
 *
 * @property K type of Entity
 */
interface HistoryDao<K> {

    /**
     * Insert to database
     *
     * @param entity Entity to insert
     */
    @Insert
    suspend fun addToHistory(entity : K)
}

