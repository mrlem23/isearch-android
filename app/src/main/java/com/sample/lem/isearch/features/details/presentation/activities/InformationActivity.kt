package com.sample.lem.isearch.features.details.presentation.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.sample.lem.isearch.R
import com.sample.lem.isearch.features.details.domain.models.DetailsMediaItem
import com.sample.lem.isearch.features.details.domain.viewmodels.ViewHistoryViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Displays a more detailed information about a Media
 */
@AndroidEntryPoint
class InformationActivity : AppCompatActivity() {

    private val defaultImageUrl =
        "https://static.wikia.nocookie.net/1-2-3-slaughter-me-street/images/1/14/No_Image_Available.jpg/revision/latest?cb=20190212031835"

    // Lazily initialize references to views since view traversal is costly
    private val ivArtWork: ImageView by lazy { findViewById(R.id.ivArtwork) }
    private val tvPrice: TextView by lazy { findViewById(R.id.tvPrice) }
    private val tvTitle: TextView by lazy { findViewById(R.id.tvTitle) }
    private val tvGenre: TextView by lazy { findViewById(R.id.tvGenre) }
    private val tvDescription: TextView by lazy { findViewById(R.id.tvDescription) }

    private val viewHistoryViewModel by lazy {
        ViewModelProvider(this).get(ViewHistoryViewModel::class.java)
    }

    companion object {
        private val mediaItemExtraKey = "${InformationActivity::class.java.simpleName}/mediaItem"

        /**
         * Convenient function in order to make sure that needed data is passed
         *
         * @param context Activity or Service
         * @param detailsMediaItem MediaItem
         */
        fun push(context: Context, detailsMediaItem: DetailsMediaItem) {
            Intent(context, InformationActivity::class.java).apply {
                // Model is converted to String, this no longer requires model to be
                // Serializable or Parcelable
                putExtra(mediaItemExtraKey, Gson().toJson(detailsMediaItem))
                context.startActivity(this)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_information)
        intent.getStringExtra(mediaItemExtraKey)?.also { mediaItemExtra ->
            // Model is converted back to its Model using Gson
            Gson().fromJson(mediaItemExtra, DetailsMediaItem::class.java).also { mediaItem ->
                viewHistoryViewModel.saveViewToHistory(mediaItem)
                inflateView(mediaItem)
            }
        }
    }

    /**
     * Inflate view with
     *
     * @param detailsMediaItem extra passed via intent
     */
    private fun inflateView(detailsMediaItem: DetailsMediaItem) {
        RequestOptions().error(R.drawable.bg_error)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH).also { requestOption ->
                Glide.with(this).load(detailsMediaItem.imgUrl ?: defaultImageUrl).apply(requestOption).into(ivArtWork)
            }
        tvPrice.text = detailsMediaItem.price
        tvTitle.text = detailsMediaItem.title
        tvGenre.text = detailsMediaItem.genre
        tvDescription.text = detailsMediaItem.description
    }
}