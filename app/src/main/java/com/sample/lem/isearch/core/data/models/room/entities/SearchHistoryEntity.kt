package com.sample.lem.isearch.core.data.models.room.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Stores search history of user
 */
@Entity(tableName = "search_history")
data class SearchHistoryEntity (
    @PrimaryKey(autoGenerate = true)
    val id : Int? = null,
    val query : String
)