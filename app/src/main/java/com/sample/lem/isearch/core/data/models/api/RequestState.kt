package com.sample.lem.isearch.core.data.models.api

/**
 * State of request
 *
 * @property Loading Request is still ongoing
 * @property Success Request is finished and was successful
 * @property Failed Request is finished but there was some error
 */
enum class RequestState {
    Loading, Success, Failed
}