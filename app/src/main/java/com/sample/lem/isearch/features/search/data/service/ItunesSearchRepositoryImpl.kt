package com.sample.lem.isearch.features.search.data.service

import com.sample.lem.isearch.features.search.data.models.api.SearchResultResponse
import retrofit2.Response
import javax.inject.Inject

/**
 * Concrete implementation of [com.sample.lem.isearch.features.search.data.service.ItunesSearchRepository]
 *
 * @param itunesSearchService service layer that calls the iTunes API
 */
class ItunesSearchRepositoryImpl @Inject constructor(
    private val itunesSearchService: ItunesSearchService
) : ItunesSearchRepository{

    override suspend fun search(
        query : String,
        country : String,
        media : String
    ): Response<SearchResultResponse> = itunesSearchService.searchTerm(query, country, media)
}