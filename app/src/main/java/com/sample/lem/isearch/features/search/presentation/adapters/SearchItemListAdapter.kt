package com.sample.lem.isearch.features.search.presentation.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.sample.lem.isearch.R
import com.sample.lem.isearch.features.search.domain.model.SearchResultItem

/**
 * Item adapter for view [com.sample.lem.isearch.R.layout.item_card_result]
 *
 * @param resultList list of [com.sample.lem.isearch.features.search.domain.model.SearchResultItem]
 * @param onItemClick lambda function called when an item is clicked
 */
class SearchItemListAdapter(
    private val resultList: List<SearchResultItem>,
    private val onItemClick : (item : SearchResultItem) -> Unit
) : RecyclerView.Adapter<SearchItemListAdapter.SearchItemListViewHolder>() {

    // Default img url if imgUrl is null
    private val defaultImageUrl =
        "https://static.wikia.nocookie.net/1-2-3-slaughter-me-street/images/1/14/No_Image_Available.jpg/revision/latest?cb=20190212031835"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchItemListViewHolder =
        LayoutInflater.from(parent.context).run {
            inflate(R.layout.item_card_result, parent, false)
        }.run {
            SearchItemListViewHolder(parent.context, this)
        }

    override fun onBindViewHolder(holder: SearchItemListViewHolder, position: Int) {
        resultList[position].also { item ->
            RequestOptions().error(R.drawable.bg_error)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH).also { requestOption ->
                    Glide.with(holder.context).load(item.previewUrl ?: defaultImageUrl)
                        .apply(requestOption)
                        .into(holder.ivArtWork)
                }

            holder.tvPrice.text = item.price
            holder.tvTitle.text = item.title
            holder.tvGenre.text = item.subTitle
            holder.itemResultCard.setOnClickListener {
                onItemClick(item)
            }
        }
    }

    override fun getItemCount(): Int = resultList.size

    /**
     * ViewHolder for [com.sample.lem.isearch.R.layout.item_card_result]
     *
     * @param context from parentView
     * @param itemView instance of [com.sample.lem.isearch.R.layout.item_card_result]
     */
    class SearchItemListViewHolder(val context: Context, itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val itemResultCard : View by lazy { itemView.findViewById(R.id.itemResultCard) }
        val ivArtWork: ImageView by lazy { itemView.findViewById(R.id.ivArtwork) }
        val tvPrice: TextView by lazy { itemView.findViewById(R.id.tvPrice) }
        val tvTitle: TextView by lazy { itemView.findViewById(R.id.tvTitle) }
        val tvGenre: TextView by lazy { itemView.findViewById(R.id.tvGenre) }
    }
}