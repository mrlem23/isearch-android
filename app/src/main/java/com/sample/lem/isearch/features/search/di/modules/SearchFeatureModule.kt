package com.sample.lem.isearch.features.search.di.modules

import com.sample.lem.isearch.core.data.databases.ISearchDatabase
import com.sample.lem.isearch.core.data.service.RetrofitProvider
import com.sample.lem.isearch.core.data.models.room.dao.SearchDao
import com.sample.lem.isearch.features.search.data.service.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


/**
 * DI Module specifically for this sub module/feature
 * Separated in order for scope and readibily, easier to maintain too
 */
@Module
@InstallIn(SingletonComponent::class)
class SearchFeatureModule {

    /**
     * Injects an instance of [com.sample.lem.isearch.features.search.data.service.ItunesSearchRepositoryImpl]
     * to any class that needs it
     *
     * @param itunesSearchApi injected instance of [com.sample.lem.isearch.features.search.data.service.ItunesSearchRepositoryImpl]
     */
    @Provides
    @Singleton
    fun provideItunesSearchApi(itunesSearchApi: ItunesSearchRepositoryImpl): ItunesSearchRepository =
        itunesSearchApi

    /**
     * Injects an instance of [com.sample.lem.isearch.features.search.data.service.ItunesSearchService]
     * to any class that needs it
     *
     * @param retrofitProvider injected via [com.sample.lem.isearch.core.di.modules.DataModule]
     */
    @Provides
    @Singleton
    fun provideItunesSearchService(retrofitProvider: RetrofitProvider): ItunesSearchService =
        retrofitProvider.retrofit.create(ItunesSearchService::class.java)

    /**
     * Injects instance of [com.sample.lem.isearch.core.data.models.room.SearchDao] that will be used
     * for querying [com.sample.lem.isearch.core.data.models.room.SearchHistoryEntity]
     */
    @Provides
    fun provideSearchDao(iSearchDB: ISearchDatabase): SearchDao {
        return iSearchDB.searchDao()
    }

    /**
     * Provide a concrete implementation for [com.sample.lem.isearch.features.search.data.service.SearchHistoryRepository]
     */
    @Provides
    fun provideSearchHistoryRepository(searchHistoryRepositoryImpl: SearchHistoryRepositoryImpl): SearchHistoryRepository =
        searchHistoryRepositoryImpl

    /**
     * Provide a concrete implementation for [com.sample.lem.isearch.features.search.data.service.SearchViewHistoryRepository]
     */
    @Provides
    fun provideSearchViewHistoryRepository(searchViewHistoryRepository: SearchViewHistoryRepositoryImpl): SearchViewHistoryRepository =
        searchViewHistoryRepository
}