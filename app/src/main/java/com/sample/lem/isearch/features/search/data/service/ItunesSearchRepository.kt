package com.sample.lem.isearch.features.search.data.service

import com.sample.lem.isearch.features.search.data.models.api.SearchResultResponse
import retrofit2.Response

/**
 * Handles searching iTunes for a media
 */
interface ItunesSearchRepository {

    /**
     * Search itunes using the ff parameters
     *
     * @param query any string
     * @param country two digit country code [see link](http://en.wikipedia.org/wiki/%20ISO_3166-1_alpha-2)
     * @param media can be any of the ff (movie, podcast, music, musicVideo, audiobook, shortFilm, tvShow, software, ebook, all)
     */
    suspend fun search(
        query: String,
        country: String,
        media: String = "all",
    ): Response<SearchResultResponse>
}