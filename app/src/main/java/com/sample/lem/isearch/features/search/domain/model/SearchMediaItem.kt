package com.sample.lem.isearch.features.search.domain.model

/**
 * Model specific for features/search.
 * Its generally a good idea to define an individual model per submodule as this
 * removes the dependency of a module to other modules (except for the core module)
 *
 * @param imgUrl URL of image preview
 * @param price price of Media w/ currency
 * @param title
 * @param genre
 * @param description
 */
data class SearchMediaItem(
    val imgUrl :String?,
    val price :String?,
    val title : String?,
    val genre : String?,
    val description : String?,
)