package com.sample.lem.isearch.features.details.di.modules

import com.sample.lem.isearch.core.data.databases.ISearchDatabase
import com.sample.lem.isearch.features.details.data.services.ViewHistoryRepository
import com.sample.lem.isearch.features.details.data.services.ViewHistoryRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * DI Module specifically for this sub module/feature
 * Separated in order for scope and readibily, easier to maintain too
 */
@Module
@InstallIn(SingletonComponent::class)
class DetailsFeatureModule {

    /**
     * Provide instance of ViewHistoryRepository
     *
     * @param viewHistoryRepository instance of [com.sample.lem.isearch.features.details.data.services]
     */
    @Provides
    fun getViewHistoryRepository(viewHistoryRepository: ViewHistoryRepositoryImpl): ViewHistoryRepository =
        viewHistoryRepository
}