package com.sample.lem.isearch.core.data.models.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.sample.lem.isearch.core.data.models.room.entities.SearchHistoryEntity


/**
 * DAO to access [com.sample.lem.isearch.core.data.models.room.entities.SearchHistoryEntity]
 */
@Dao
interface SearchDao : HistoryDao<SearchHistoryEntity> {
    /**
     * A custom query is created in order to delete all instance of query in history instead of
     * using [androidx.room.Delete] annotation
     */
    @Query("DELETE FROM search_history WHERE `query`=:query")
    suspend fun removeFromSearchHistory(query : String)

    /**
     * Only get distinct values, and order by Desc to get latest history
     */
    @Query("SELECT DISTINCT `query` FROM search_history ORDER BY id DESC LIMIT :maxCount")
    fun getRecentHistory(maxCount : Int) : List<String>
}