package com.sample.lem.isearch.features.search.data.service

import com.sample.lem.isearch.core.data.models.room.dao.SearchDao
import com.sample.lem.isearch.core.data.models.room.entities.SearchHistoryEntity
import javax.inject.Inject

/**
 * Concrete implementation of [com.sample.lem.isearch.features.search.data.service.SearchHistoryRepository]
 *
 * @param searchHistoryDao DAO for accessing [com.sample.lem.isearch.core.data.models.room.entities.SearchHistoryEntity]
 */
class SearchHistoryRepositoryImpl @Inject constructor(
    private val searchHistoryDao: SearchDao
) : SearchHistoryRepository {
    override fun getLatestSearch(count: Int): List<String> =
        searchHistoryDao.getRecentHistory(3)

    override suspend fun saveSearch(query: String) =
        searchHistoryDao.addToHistory(SearchHistoryEntity(query = query))

    override suspend fun deleteQuery(query: String, count: Int) =
        searchHistoryDao.removeFromSearchHistory(query)
}