package com.sample.lem.isearch.core.data.models.room.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Stores search history of user
 *
 * Again we use a different model here in order to decouple and avoid dependencies
 */
@Entity(tableName = "view_history")
data class ViewHistoryEntity (
    @PrimaryKey(autoGenerate = true)
    val id : Int? = null,
    val price : String?,
    val previewUrl : String?,
    val fullImgUrl : String?,
    val title : String?,
    val subTitle : String?,
    val description : String?
)