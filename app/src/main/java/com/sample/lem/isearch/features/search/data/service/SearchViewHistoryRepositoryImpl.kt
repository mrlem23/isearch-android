package com.sample.lem.isearch.features.search.data.service

import com.sample.lem.isearch.core.data.models.room.dao.ViewHistoryDao
import com.sample.lem.isearch.features.search.domain.model.SearchMediaItem
import javax.inject.Inject

/**
 * Concrete implementation of [com.sample.lem.isearch.features.search.data.service.SearchViewHistoryRepository]
 *
 * @param searchHistoryDao DAO for accessing [com.sample.lem.isearch.core.data.models.room.entities.ViewHistoryEntity]
 */
class SearchViewHistoryRepositoryImpl @Inject constructor(
    private val viewHistoryDao: ViewHistoryDao
): SearchViewHistoryRepository {
    override suspend fun getHistory(count : Int): List<SearchMediaItem> {
        val historyDetailsMediaItem : ArrayList<SearchMediaItem> = ArrayList();

        viewHistoryDao.getRecentHistory(count).forEach { viewHistoryEntity ->
            SearchMediaItem(
                title = viewHistoryEntity.title,
                price = viewHistoryEntity.price,
                description = viewHistoryEntity.description,
                imgUrl = viewHistoryEntity.fullImgUrl,
                genre = viewHistoryEntity.subTitle
            ).apply {
                historyDetailsMediaItem.add(this)
            }
        }

        return historyDetailsMediaItem
    }
}