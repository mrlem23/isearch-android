package com.sample.lem.isearch.core.data.models.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.sample.lem.isearch.core.data.models.room.entities.ViewHistoryEntity

/**
 * DAO for [com.sample.lem.isearch.core.data.models.room.entities.ViewHistoryEntity]
 */
@Dao
interface ViewHistoryDao : HistoryDao<ViewHistoryEntity> {
    /**
     * Only get distinct values, and order by Desc to get latest history
     */
    @Query("SELECT * FROM view_history GROUP BY `fullImgUrl` ORDER BY id DESC LIMIT :maxCount")
    fun getRecentHistory(maxCount : Int) : List<ViewHistoryEntity>
}