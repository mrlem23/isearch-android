package com.sample.lem.isearch.features.search.data.models.api

/**
 * Response model from iTunes
 *
 * @param resultCount number of result
 * @param results arraylist of result
 */
data class SearchResultResponse (
    val resultCount : Int,
    val results : List<SearchItem>
)