package com.sample.lem.isearch.features.search.data.service

/**
 * Handles obtaining data for Search history
 */
interface SearchHistoryRepository {
    /**
     * Get latest history base on
     *
     * @param count
     */
    fun getLatestSearch(
        count : Int
    ) : List<String>

    /**
     * Persistently save the following term in search history
     */
    suspend fun saveSearch(
        query : String
    )

    /**
     * Remove the following term in search history
     */
    suspend fun deleteQuery(
        query: String,
        count: Int
    )
}