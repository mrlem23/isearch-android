package com.sample.lem.isearch.features.details.data.services

import com.sample.lem.isearch.core.data.models.room.dao.ViewHistoryDao
import com.sample.lem.isearch.core.data.models.room.entities.ViewHistoryEntity
import com.sample.lem.isearch.features.details.domain.models.DetailsMediaItem
import javax.inject.Inject

class ViewHistoryRepositoryImpl @Inject constructor(
    private val viewHistoryDao: ViewHistoryDao
) : ViewHistoryRepository {

    /**
     * Save this view to history
     *
     * @param detailsMediaItem data model for presentation layer, this is passed again to avoid dependencies
     */
    override suspend fun saveToViewHistory(detailsMediaItem: DetailsMediaItem) {
        viewHistoryDao.addToHistory(
            ViewHistoryEntity(
                description = detailsMediaItem.description ?: "",
                fullImgUrl = detailsMediaItem.imgUrl ?: "",
                previewUrl = detailsMediaItem.imgUrl ?: "",
                price = detailsMediaItem.price ?: "",
                subTitle = detailsMediaItem.genre ?: "",
                title = detailsMediaItem.title ?: ""
            )
        )
    }
}