package com.sample.lem.isearch.features.search.data.service

import com.sample.lem.isearch.features.search.data.models.api.SearchResultResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Retrofit interface
 */
interface ItunesSearchService {
    @GET("/search")
    suspend fun searchTerm(
        @Query("term") query : String,
        @Query("country") country : String,
        @Query("media") media : String = "all",
    ) : Response<SearchResultResponse>
}