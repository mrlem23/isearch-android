package com.sample.lem.isearch.core.di.modules

import android.content.Context
import androidx.room.Room
import androidx.room.migration.Migration
import com.sample.lem.isearch.BuildConfig
import com.sample.lem.isearch.core.data.databases.ISearchDatabase
import com.sample.lem.isearch.core.data.service.RetrofitProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


/**
 * Application and multi module needed dependencies
 */
@Module
@InstallIn(SingletonComponent::class)
class DataModule {

    // This will provide a consistent Retrofit instance for all services
    // Set as Singleton to as there is no need for multiple instances
    @Provides
    @Singleton
    fun provideRetrofit() = RetrofitProvider(BuildConfig.BASE_URL)

    @Provides
    @Singleton
    fun provideISearchDatabase(
        @ApplicationContext context: Context
    ): ISearchDatabase =
        Room.databaseBuilder(context, ISearchDatabase::class.java, ISearchDatabase.key)
            .fallbackToDestructiveMigration().build()

    /**
     * ViewHistoryDao is being accessed in multiple submoduls so it make sense to place this in core
     */
    @Provides
    fun getViewHistoryDao(iSearchDatabase: ISearchDatabase) = iSearchDatabase.viewHistoryDao()
}