package com.sample.lem.isearch.features.search.domain.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.sample.lem.isearch.features.details.domain.models.DetailsMediaItem
import com.sample.lem.isearch.features.search.data.service.SearchViewHistoryRepository
import com.sample.lem.isearch.features.search.domain.model.SearchMediaItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewHistoryViewModel @Inject constructor(
    application: Application,
    private val viewHistoryRepository: SearchViewHistoryRepository
) : AndroidViewModel(application) {

    /**
     * No need to return anything as this is a silent save
     *
     * @param mediaItem presentation model
     */
    fun getHistory(count: Int) : LiveData<List<SearchMediaItem>> {
        val viewHistory : MutableLiveData<List<SearchMediaItem>> = MutableLiveData()

        viewModelScope.launch(Dispatchers.IO){
            viewHistory.postValue(viewHistoryRepository.getHistory(3))
        }

        return viewHistory
    }
}