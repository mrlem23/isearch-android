package com.sample.lem.isearch.features.search.presentation.views

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sample.lem.isearch.R
import com.sample.lem.isearch.features.search.domain.model.SearchMediaItem
import com.sample.lem.isearch.features.search.presentation.adapters.ViewHistoryViewAdapter

/**
 * Compound view which shows list of last view horizontally
 */
class ViewHistoryListView(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
    ConstraintLayout(context, attrs, defStyleAttr) {

    constructor(context: Context) : this(context, null, 0)

    constructor(context: Context, attrs: AttributeSet) : this(context, attrs, 0)

    /**
     * Lambda function called when an item is clicked
     *
     * @param query
     */
    private var onHistoryItemClicked: (searchMediaItem: SearchMediaItem) -> Unit = {}

    /**
     * Recycler view that holds the horizontal grid
     */
    private val rvHistoryList: RecyclerView by lazy { findViewById(R.id.rvViewHistory) }

    init {
        inflate(context, R.layout.view_history_list, this)

        rvHistoryList.adapter = ViewHistoryViewAdapter(arrayListOf(), onHistoryItemClicked)
        rvHistoryList.layoutManager =
            GridLayoutManager(context, 3, LinearLayoutManager.VERTICAL, false)
    }

    /**
     * Update data of Recyclerview, automatically hides or show this view if data is empty or not
     */
    fun updateHistoryList(newList : List<SearchMediaItem>) {
        if(newList.isEmpty()) {
            visibility = GONE
        } else {
            visibility = VISIBLE
            rvHistoryList.adapter = ViewHistoryViewAdapter(newList, onHistoryItemClicked)
        }
    }

    /**
     * Set/Override default [com.sample.lem.isearch.features.search.presentation.views.ViewHistoryListView.onHistoryItemClicked]
     */
    fun setOnHistoryItemClicked(listener: (searchMediaItem: SearchMediaItem) -> Unit) {
        onHistoryItemClicked = listener
    }
}