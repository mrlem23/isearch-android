package com.sample.lem.isearch.features.details.data.services

import com.sample.lem.isearch.features.details.domain.models.DetailsMediaItem


interface ViewHistoryRepository {
    /**
     * Insert data to ViewHistory
     */
    suspend fun saveToViewHistory(detailsMediaItem : DetailsMediaItem)
}