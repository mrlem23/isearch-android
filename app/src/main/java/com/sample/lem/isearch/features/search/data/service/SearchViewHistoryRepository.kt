package com.sample.lem.isearch.features.search.data.service

import com.sample.lem.isearch.features.search.domain.model.SearchMediaItem

/**
 * Handles obtaining data for View history
 */
interface SearchViewHistoryRepository {
    /**
    * Get latest history base on
    *
    * @param count
    */
    suspend fun getHistory(count: Int) : List<SearchMediaItem>
}