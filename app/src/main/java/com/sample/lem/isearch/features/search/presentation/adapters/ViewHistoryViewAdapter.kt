package com.sample.lem.isearch.features.search.presentation.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.sample.lem.isearch.R
import com.sample.lem.isearch.features.search.domain.model.SearchMediaItem

/**
 * @param viewHistory
 * @param onItemClickListener called
 */
class ViewHistoryViewAdapter(
    private val viewHistory : List<SearchMediaItem>,
    private val onItemClickListener: (searchMediaItem : SearchMediaItem) -> Unit
) : RecyclerView.Adapter<ViewHistoryViewAdapter.ViewHistoryItemViewHolder>() {

    // Default img url if imgUrl is null
    private val defaultImageUrl =
        "https://static.wikia.nocookie.net/1-2-3-slaughter-me-street/images/1/14/No_Image_Available.jpg/revision/latest?cb=20190212031835"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHistoryItemViewHolder =
        parent.context.let { context ->
            LayoutInflater.from(context).inflate(R.layout.item_view_history, parent, false).run {
                ViewHistoryItemViewHolder(context, this)
            }
        }

    override fun onBindViewHolder(holder: ViewHistoryItemViewHolder, position: Int) {
        viewHistory[position].also { mediaItem ->
            holder.tvTitle.text = mediaItem.title

            RequestOptions().error(R.drawable.bg_error)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH).also { requestOption ->
                    Glide.with(holder.context).load(mediaItem.imgUrl ?: defaultImageUrl)
                        .apply(requestOption)
                        .into(holder.ivArtWork)
                }

            holder.viewContainer.setOnClickListener {
                onItemClickListener(mediaItem)
            }
        }
    }

    override fun getItemCount(): Int = viewHistory.size

    class ViewHistoryItemViewHolder(val context: Context, itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val tvTitle: TextView by lazy { itemView.findViewById(R.id.tvTitle) }
        val ivArtWork: ImageView by lazy { itemView.findViewById(R.id.ivArtwork) }
        val viewContainer : View by lazy { itemView.findViewById(R.id.container) }
    }
}