package com.sample.lem.isearch.core.data.databases

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sample.lem.isearch.core.data.models.room.dao.SearchDao
import com.sample.lem.isearch.core.data.models.room.dao.ViewHistoryDao
import com.sample.lem.isearch.core.data.models.room.entities.SearchHistoryEntity
import com.sample.lem.isearch.core.data.models.room.entities.ViewHistoryEntity

/**
 * Main Database for app
 */
@Database(entities = [SearchHistoryEntity::class, ViewHistoryEntity::class], version = 2)
abstract class ISearchDatabase : RoomDatabase() {
    companion object {
        const val key: String = "isearch.db";
    }

    /**
     * Provide an instance of [com.sample.lem.isearch.core.data.models.room.dao.SearchDao]
     */
    abstract fun searchDao() : SearchDao

    /**
     * Provide an instance of [com.sample.lem.isearch.core.data.models.room.dao.ViewHistoryDao]
     */
    abstract fun viewHistoryDao() : ViewHistoryDao
}