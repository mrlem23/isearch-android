package com.sample.lem.isearch.features.search.presentation.activities

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.sample.lem.isearch.R
import com.sample.lem.isearch.core.data.models.api.RequestState
import com.sample.lem.isearch.core.presentation.activities.dismissKeyboard
import com.sample.lem.isearch.features.details.domain.models.DetailsMediaItem
import com.sample.lem.isearch.features.details.presentation.activities.InformationActivity
import com.sample.lem.isearch.features.search.domain.model.SearchResultItem
import com.sample.lem.isearch.features.search.domain.viewmodels.SearchViewModel
import com.sample.lem.isearch.features.search.domain.viewmodels.SearchViewHistoryViewModel
import com.sample.lem.isearch.features.search.presentation.views.SearchHistoryListView
import com.sample.lem.isearch.features.search.presentation.adapters.SearchItemListAdapter
import com.sample.lem.isearch.features.search.presentation.views.ViewHistoryListView
import dagger.hilt.android.AndroidEntryPoint

/**
 * MainActivity of the App
 *
 * Default data used for [com.sample.lem.isearch.features.search.domain.viewmodels.SearchViewModel.search] is star and au
 */
@AndroidEntryPoint
class SearchActivity : AppCompatActivity() {

    // Lazily initialize references to views since view traversal is costly
    private val loaderView: View by lazy { findViewById(R.id.loader) }
    private val etSearchBox: EditText by lazy { findViewById(R.id.etSearchBox) }
    private val resultListRecyclerView: RecyclerView by lazy { findViewById(R.id.rvResults) }
    private val searchHistoryListView: SearchHistoryListView by lazy { findViewById(R.id.searchHistoryList) }
    private val viewHistoryList: ViewHistoryListView by lazy { findViewById(R.id.viewHistoryList) }

    // Obtain SearchResultViewModel instance
    // There is only one instance of SearchResultViewModel for the whole lifetime of the App
    private val searchViewModel: SearchViewModel by lazy {
        ViewModelProvider(this).get(SearchViewModel::class.java)
    }

    // Obtain SearchResultViewModel instance
    // There is only one instance of SearchResultViewModel for the whole lifetime of the App
    private val searchSearchViewHistoryViewModel: SearchViewHistoryViewModel by lazy {
        ViewModelProvider(this).get(SearchViewHistoryViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        listenToSearchAction()
        setupViews()
        setupRecyclerView()
        search("star")
    }

    /**
     * Prepare views for changes and updates
     */
    private fun setupViews() {
        etSearchBox.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                searchSearchViewHistoryViewModel.getHistory(3).observe(this, { historyList ->
                    viewHistoryList.updateHistoryList(historyList)
                })
                searchViewModel.getSearchHistory(3).observe(this, { searchHistory ->
                    searchHistoryListView.updateData(searchHistory)
                })
            } else {
                viewHistoryList.visibility = View.GONE
                searchHistoryListView.visibility = View.GONE
            }
        }

        viewHistoryList.setOnHistoryItemClicked { searchMediaItem ->
            InformationActivity.push(
                this, DetailsMediaItem(
                    imgUrl = searchMediaItem.imgUrl,
                    price = searchMediaItem.price,
                    title = searchMediaItem.title,
                    genre = searchMediaItem.genre,
                    description = searchMediaItem.description
                )
            )
        }

        searchHistoryListView.setOnItemClicked { query ->
            etSearchBox.setText(query)
            search(query)
        }

        searchHistoryListView.setDeleteItemClicked { query ->
            searchViewModel.removeFromSearchHistory(query).observe(this, { newList ->
                searchHistoryListView.updateData(newList)
            })
        }
    }

    /**
     * Trigger search via viewmodel
     *
     * @param query term to use on request
     */
    private fun search(query: String) {
        dismissKeyboard()
        etSearchBox.clearFocus()
        showViewDependingOnState(RequestState.Loading)

        searchViewModel.search(query, "au").observe(this,
            { result ->
                showViewDependingOnState(result.state)
                when (result.state) {
                    RequestState.Success -> {
                        updateListAdapter(result.data)
                    }
                    RequestState.Failed -> {
                        // Not implemented for now
                    }
                    RequestState.Loading -> {
                        // Not implemented for now
                    }
                }
            })
    }

    /**
     * Updates data for search results
     */
    private fun updateListAdapter(newList: List<SearchResultItem>) {
        resultListRecyclerView.adapter =
            SearchItemListAdapter(newList, onSearchResultClicked)
    }

    /**
     * Triggered upon [android.view.inputmethod.EditorInfo.IME_ACTION_SEARCH] is pressed
     */
    private fun listenToSearchAction() {
        etSearchBox.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH &&
                    etSearchBox.text.trim().isNotEmpty()
                ) {
                    search(etSearchBox.text.toString())
                    return true
                }
                return false
            }
        })
    }

    /**
     * Sets up RecyclerView
     * Only called once on activity's onCreate
     */
    private fun setupRecyclerView() {
        resultListRecyclerView.adapter = SearchItemListAdapter(arrayListOf(), onSearchResultClicked)
        resultListRecyclerView.layoutManager =
            StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)
    }

    /**
     * Update visibility of views depending on
     *
     * @param state the most recent state of [com.sample.lem.isearch.features.search.domain.viewmodels.SearchViewModel.requestState]
     */
    private fun showViewDependingOnState(state: RequestState) {
        when (state) {
            RequestState.Success -> {
                loaderView.visibility = View.GONE
            }
            RequestState.Loading -> {
                loaderView.visibility = View.VISIBLE
            }
            RequestState.Failed -> {
                loaderView.visibility = View.GONE
            }
        }
    }

    /**
     * Handles action when an item is clicked on the list
     *
     * Lambda function passed to [com.sample.lem.isearch.features.search.presentation.adapters.SearchItemListAdapter]
     *
     * @param item the item that was clicked
     */
    private val onSearchResultClicked = fun(item: SearchResultItem) {
        etSearchBox.clearFocus()
        InformationActivity.push(
            this, DetailsMediaItem(
                description = item.description,
                genre = item.subTitle,
                price = item.price,
                imgUrl = item.fullImgUrl,
                title = item.title
            )
        )
    }
}