package com.sample.lem.isearch.features.details.domain.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.sample.lem.isearch.features.details.data.services.ViewHistoryRepository
import com.sample.lem.isearch.features.details.domain.models.DetailsMediaItem
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ViewHistoryViewModel @Inject constructor(
    application: Application,
    private val viewHistoryRepository: ViewHistoryRepository
) : AndroidViewModel(application) {

    /**
     * No need to return anything as this is a silent save
     *
     * @param detailsMediaItem presentation model
     */
    fun saveViewToHistory(detailsMediaItem : DetailsMediaItem){
        viewModelScope.launch(Dispatchers.IO){
            viewHistoryRepository.saveToViewHistory(detailsMediaItem = detailsMediaItem)
        }
    }
}